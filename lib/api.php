<?php

include "cars.php";
	// formatAgg turns agg mongo format to JSON one
function formatAgg($agg,$field) {
		$ret = array();
		foreach ($agg["retval"] as $val) {
			if ($val[$field] != "") {
				$ret[$val[$field]] = $val["count"];
			}
			
		}
		return $ret;
	}

class APICar{

	static function GetCarList($db,$collection){
		$cars = new Cars();

		$client = new MongoClient(MONGO_HOST);
		$cars = $cars->Get($client,$db,$collection);
		echo json_encode($cars);
	}

	static function CreateNewCar($db,$collection){
		$car = new Car();

		$client = new MongoClient(MONGO_HOST);
		$car = $car->Create($client,$db,$collection);
		echo json_encode($car);
	}

	static function DeleteCar($db,$collection,$id){
		$car = new Car();

		$client = new MongoClient(MONGO_HOST);
		$car->Delete($client,$db,$collection,$id);
		echo json_encode(array('success' => 'deleted'));
	}

	static function GetACar($db,$collection,$id){
		$car = new Car();
		$client = new MongoClient(MONGO_HOST);
		$car = $car->Get($client,$db,$collection,$id);
		echo json_encode($car);
	}

	static function GetExteriorColors($db,$collection){
		$select = array(
		'filter'	=> (isset($_GET['filter']))		? $_GET['filter']	: false,
	  );
		$criteria = array();
		$criteria = GetQueryFilter($criteria, $select);

		$car = new Cars();
		$client = new MongoClient(MONGO_HOST);
		$agg = $car->GetExteriorColors($client,$db,$collection,$criteria);

		$ret = array();
		$ret["exterior_color"] = formatAgg($agg,"exterior_color");

		$colors = explode(",", "Beige,Black,Blue,Brown,Burgundy,Charcoal,Gold,Gray,Green,Orange,Pink,Purple,Red,Silver,Tan,Turquoise,White,Yellow");

		$final = array();
		foreach ($colors as $src_color) {
			foreach ($ret["exterior_color"] as $color => $count) {
				if (preg_match("/(.*)?$src_color(.*)?/i", $color)) {
					if (isset($final[$src_color])) {
						$final[$src_color] = $final[$src_color] + $count;
					} else{
						$final[$src_color] = $count;
					}
				}
			}
		}

		// print_r($ret);

		return array("exterior_colors"=>$final);

	}

	static function GetInteriorColors($db,$collection){
		$select = array(
		'filter'	=> (isset($_GET['filter']))		? $_GET['filter']	: false,
	  );
		$criteria = array();
		$criteria = GetQueryFilter($criteria, $select);

		$car = new Cars();
		$client = new MongoClient(MONGO_HOST);
		$agg = $car->GetInteriorColors($client,$db,$collection,$criteria);

		$ret = array();
		$ret["interior_color"] = formatAgg($agg,"interior_color");

		$colors = explode(",", "Beige,Black,Blue,Brown,Burgundy,Charcoal,Gold,Gray,Green,Orange,Pink,Purple,Red,Silver,Tan,Turquoise,White,Yellow");

		$final = array();
		foreach ($colors as $src_color) {
			foreach ($ret["interior_color"] as $color => $count) {
				if (preg_match("/(.*)?$src_color(.*)?/i", $color)) {
					if (isset($final[$src_color])) {
						$final[$src_color] = $final[$src_color] + $count;
					} else{
						$final[$src_color] = $count;
					}
				}
			}
		}

		return array("interior_colors"=>$final);

	}

	static function GetMakes($db,$collection){
		$car = new Cars();
		$client = new MongoClient(MONGO_HOST);
		$agg = $car->GetMakes($client,$db,$collection);

		$ret = array();
		$ret["makes"] = formatAgg($agg,"make");

		echo json_encode($ret);

	}

	static function GetModels($db,$collection){
		$car = new Cars();
		$client = new MongoClient(MONGO_HOST);
		$agg = $car->GetModels($client,$db,$collection);
		$ret = array();
		$ret["models"] = formatAgg($agg,"model");
		echo json_encode($ret);
	}

	static function GetFuel($db,$collection){
		$select = array(
		'filter'	=> (isset($_GET['filter']))		? $_GET['filter']	: false,
	  );
		$criteria = array();
		$criteria = GetQueryFilter($criteria, $select);

		$car = new Cars();
		$client = new MongoClient(MONGO_HOST);
		$agg = $car->GetFuel($client,$db,$collection,$criteria);
		$ret = array();
		$ret["fuel"] = formatAgg($agg,"fuel_type");
		return $ret;
	}

	static function GetTrim($db,$collection){
		$select = array(
		'filter'	=> (isset($_GET['filter']))		? $_GET['filter']	: false,
	  );
		$criteria = array();
		$criteria = GetQueryFilter($criteria, $select);

		$car = new Cars();
		$client = new MongoClient(MONGO_HOST);
		$agg = $car->GetTrim($client,$db,$collection,$criteria);
		$ret = array();
		$ret["trims"] = formatAgg($agg,"trim");

		arsort($ret["trims"]);
		return $ret;
	}

	static function GetTransmissions($db,$collection){
		$select = array(
		'filter'	=> (isset($_GET['filter']))		? $_GET['filter']	: false,
	  );
		$criteria = array();
		$criteria = GetQueryFilter($criteria, $select);

		$car = new Cars();
		$client = new MongoClient(MONGO_HOST);
		$agg = $car->GetTransmissions($client,$db,$collection,$criteria );
		$ret = array();
		$ret["transmissions"] = formatAgg($agg,"transmission");

		$final = array();
		$final["A/T;Automatic;Continuously"] = 0;
		$final["Manual"] = 0;
		foreach ($ret["transmissions"] as $transmission => $count) {
			if ($transmission == "5-Speed Manual") {
				$final["Manual"] += $count;
			} else {
				$final["A/T;Automatic;Continuously"] += $count;
			}
		}

		foreach ($final as $key => $value) {
			if ($final[$key] == 0) {
				unset($final[$key]);
			}
		}

		return array("transmissions"=>$final);;
	}

	static function GetDriveTrain($db,$collection){
		$select = array(
		'filter'	=> (isset($_GET['filter']))		? $_GET['filter']	: false,
	  );
		$criteria = array();
		$criteria = GetQueryFilter($criteria, $select);

		$car = new Cars();
		$client = new MongoClient(MONGO_HOST);
		$agg = $car->GetDriveTrain($client,$db,$collection,$criteria);
		$ret = array();
		$ret["drive_trains"] = formatAgg($agg,"drive_train");


		$final = array();
		$final["4WD;4WD:DRW;AWD"] = 0;
		$final["FWD"] = 0;
		$final["RWD"] = 0;
		foreach ($ret["drive_trains"] as $drive_type => $count) {
			switch ($drive_type) {
				case '4WD':
					$final["4WD;4WD:DRW;AWD"] += $count;
					break;
				case '4WD:DRW':
					$final["4WD;4WD:DRW;AWD"] += $count;
					break;
				case 'AWD':
					$final["4WD;4WD:DRW;AWD"] += $count;
					break;
				case 'FWD':
					$final["FWD"] += $count;
					break;
				case 'RWD':
					$final["RWD"] += $count;
					break;
				
				default:
					# code...
					break;
			}
		}

		foreach ($final as $key => $value) {
			if ($final[$key] == 0) {
				unset($final[$key]);
			}
		}
		return array("drive_trains"=>$final);
	}

	static function GetEngine($db,$collection){
		$select = array(
		'filter'	=> (isset($_GET['filter']))		? $_GET['filter']	: false,
	  );
		$criteria = array();
		$criteria = GetQueryFilter($criteria, $select);

		$car = new Cars();
		$client = new MongoClient(MONGO_HOST);
		$agg = $car->GetEngine($client,$db,$collection,$criteria);
		$ret = array();
		$ret["engines"] = formatAgg($agg,"engine_description");

		$engines = explode(",", "3 Cyl.,4 Cyl.,5 Cyl.,6 Cyl.,8 Cyl.,10 Cyl.,12 Cyl.");

		$final = array();
		foreach ($engines as $scr_engine) {
			foreach ($ret["engines"] as $engine => $count) {
				if (preg_match("/(.*)?$scr_engine(.*)?/i", $engine)) {
					if (isset($final[$scr_engine])) {
						$final[$scr_engine] = $final[$scr_engine] + $count;
					} else{
						$final[$scr_engine] = $count;
					}
				}
			}
		}
		return array("engines"=>$final);
	}

	static function GetDoor($db,$collection){
		$select = array(
		'filter'	=> (isset($_GET['filter']))		? $_GET['filter']	: false,
	  );
		$criteria = array();
		$criteria = GetQueryFilter($criteria, $select);

		$car = new Cars();
		$client = new MongoClient(MONGO_HOST);
		$agg = $car->GetDoor($client,$db,$collection,$criteria);
		$ret = array();
		$ret["doors"] = formatAgg($agg,"doors");
		return $ret;
	}

	static function GetType($db,$collection){
		$select = array(
		'filter'	=> (isset($_GET['filter']))		? $_GET['filter']	: false,
	  );
		$criteria = array();
		$criteria = GetQueryFilter($criteria, $select);

		$car = new Cars();
		$client = new MongoClient(MONGO_HOST);
		$agg = $car->GetType($client,$db,$collection,$criteria);
		$ret = array();
		$ret["types"] = $agg;
		return $ret;
	}

	static function GetSideBar($db,$collection){

		$ret = array();

		$select = array(
		'filter'	=> (isset($_GET['filter']))		? $_GET['filter']	: false,
	  );
		$criteria = array();
		$criteria = GetQueryFilter($criteria, $select);

		$exterior_colors = APICar::GetExteriorColors($db,$collection,$criteria);
		$ret["exterior_color"] = $exterior_colors["exterior_colors"];

		$interior_colors = APICar::GetInteriorColors($db,$collection,$criteria);
		$ret["interior_color"] = $interior_colors["interior_colors"];

		$fuels = APICar::GetFuel($db,$collection,$criteria);
		$ret["fuel_type"] = $fuels["fuel"];

		$trims = APICar::GetTrim($db,$collection,$criteria);
		$ret["trim"] = $trims["trims"];

		$transmissions = APICar::GetTransmissions($db,$collection,$criteria);
		$ret["transmission"] = $transmissions["transmissions"];

		$engines = APICar::GetEngine($db,$collection,$criteria);
		$ret["engine_description"] = $engines["engines"];

		$trains = APICar::GetDriveTrain($db,$collection,$criteria);
		$ret["drive_train"] = $trains["drive_trains"];

		$doors = APICar::GetDoor($db,$collection,$criteria);
		$ret["doors"] = $doors["doors"];

		$types = APICar::GetType($db,$collection,$criteria);
		$ret["type"] = $types["types"];

		echo json_encode($ret);

	}

	static function GetAllModelsByMake($db,$collection,$make){
		$car = new Cars();
		$client = new MongoClient(MONGO_HOST);
		$agg = $car->GetAllModelsByMake($client,$db,$collection,$make);
		$ret = array();


		foreach ($agg["retval"][0]["models"] as $model) {
			if (isset($ret[$model])) {
				$ret[$model] = $ret[$model] + 1;
			} else {
				$ret[$model] = 1;
			}
		}
		arsort($ret);
		return array("make"=>$make,"models"=>$ret);
	}


	static function GetMenuForFordModels($db,$collection){
		$car = new Cars();
		$client = new MongoClient(MONGO_HOST);
		$agg = $car->GetMenuForFordModels($client,$db,$collection,"Ford");
		$ret = array();


		foreach ($agg["retval"][0]["models"] as $model) {
			if (isset($ret[$model])) {
				$ret[$model] = $ret[$model] + 1;
			} else {
				$ret[$model] = 1;
			}
		}
		arsort($ret);

		$final  = array(
			"F-150" => 0,
			"Fusion" => 0,
			"Focus" =>0,
			"Escape" => 0
		);
		foreach ($ret as $key => $count) {
			if (preg_match('/(.*)?F-150(.*)?/i', $key)) {
				$final["F-150"] += $count;
			}
			if (preg_match('/(.*)?Fusion(.*)?/i', $key)) {
				$final["Fusion"] += $count;
			}
			if (preg_match('/(.*)?Focus(.*)?/i', $key)) {
				$final["Focus"] += $count;
			}
			if (preg_match('/(.*)?Escape(.*)?/i', $key)) {
				$final["Escape"] += $count;
			}
		}
		return array("make"=>"Ford","models"=>$final);
	}

	static function GetListByIds($db,$collection,$ids){
		$cars = new Cars();
		$client = new MongoClient(MONGO_HOST);
		$ret = $cars->GetListByIds($client,$db,$collection,$ids);


		return $ret;
	}

	static function GetModelList($db,$collection){
		$car = new Cars();
		$client = new MongoClient(MONGO_HOST);
		$agg = $car->GetModels($client,$db,$collection);
		$ret = array();
		$tmp = formatAgg($agg,"model");
		foreach ($tmp as $key => $value) {
			if (is_string($key)) {
				array_push($ret, $key);
			 
			}
		}
		echo json_encode($ret);
	}

}

?>