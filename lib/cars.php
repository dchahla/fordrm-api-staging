<?php

function GetQueryFilter($criteria, $select) {
  
  // Determine Filter Options (before query)
  // if(isset($select['filter']) && count($select['filter']))
  if ($select['filter']) {
   if (array_key_exists('_id', $select['filter'])) {
    
    // Format IDs to MongoIDs before querying database
    if (is_array($select['filter']['_id'])) {
     $criteria = array('_id' => array('$in' => array()));
     
     foreach ($select['filter']['_id'] as $id) {
      array_push($criteria['_id']['$in'], new MongoId($id));
     }
    } else {
     $id = $select['filter']['_id'];
     $criteria = array('_id' => new MongoId($id));
    }
   } else {
    
    // Format numbers as integers
    $filters = $select['filter'];
    foreach ($filters as $key => $value) {
     if ($key == 'distance') {
      $location = array((float)$value['location'][0], (float)$value['location'][1]);
      $range = (float)$value['range'] / 111.12;
      
      $criteria['location'] = array('$within' => array('$center' => array($location, $range)
      
      // Note 111.12 = km per cartesian degree, which is what
      // the MongoDB 2dSphere location uses when filtering
      // by distance, or radius from center point.
      ));
     } else {
      if (is_array($value)) {
       $new_value = array();
       foreach ($value as $vkey => $vvalue) {
        if (is_numeric($vvalue)) {
         $vvalue = (int)$vvalue;
        }
        
        $new_value[$vkey] = $vvalue;
       }
       
       $value = $new_value;
      } else {
       if (is_numeric($value)) {
        $value = (int)$value;
       }
      }
      
      $criteria[$key] = $value;
     }
    }
   }
  }
  
  return $criteria;
 }

function GetQuerySearch($criteria, $select) {
  if ($select['search']) {
    // $search_elements = explode(",", $select['search'] );
   foreach ($select['search']  as $key => $values) {
    // $criteria[$key] = new MongoRegex("/(.*)?".$value."(.*)?/");
    $values = explode(",", $values);
    foreach ($values as $value) {
     if (!isset($criteria['$or'])) {
      $criteria['$or'] = array(array("$key"=>array('$regex' => new MongoRegex("/(.*)?$value(.*)?/i"))));
    } else {
       array_push($criteria['$or'], array("$key"=>array('$regex' => new MongoRegex("/(.*)?$value(.*)?/i"))));
    }
    }
   }
  }
  return $criteria;
 }

class Car{

	function Create($client,$db,$collection){
		$document = json_decode(Slim::getInstance()->request()->getBody(), true);
		$client->$db->$collection->insert($document);
		$document['_id'] = $document['_id']->{'$id'};
		return $document;
	}

	function Delete($client,$db,$collection,$id){
		$criteria = array('_id' => new MongoId($id));
		$client->$db->$collection->remove($criteria, array('w' => true));
		
	}

	function Get($client,$db,$collection,$id){
		$criteria = array('_id' => new MongoId($id));
		$document = $client->$db->$collection->findOne($criteria);
		if (isset($document['_id']->{'$id'})) {
			$document['_id'] = $document['_id']->{'$id'};
		} else {
			$document['_id'] = $document['_id'];
		}
		return $document;
	}

}


class Cars{
	function Get($client,$db,$collection){
		$limit = (isset($_GET['limit']))		? $_GET['limit']	: 10;
		$page = (isset($_GET['page']))		? $_GET['page']	: 1;


		$select = array(
		'filter'	=> (isset($_GET['filter']))		? $_GET['filter']	: false,
    'search'  => (isset($_GET['search']))   ? $_GET['search'] : false,
	  );

		$criteria = array();



		$criteria = GetQueryFilter($criteria, $select);
    // $criteria = GetQuerySearch($criteria, $select);

  

    // echo "<pre>";
    // print_r($criteria);
    // echo "</pre>";


    // check if certified is checked
    if (isset($criteria["certified"]) && ($criteria["certified"] == "true")) {
      $criteria["certified"]  = true;
    }

    // check if one_owner is checked
    if (isset($criteria["one_owner"]) && ($criteria["one_owner"] == "true")) {
      $criteria["one_owner"]  = true;
    }

    // check if low_miles is checked
    if (isset($criteria["low_miles"]) && ($criteria["low_miles"] == "true")) {
      $criteria["low_miles"]  = true;
    }

    // check if ecoboost is checked
    if (isset($criteria["ecoboost"]) && ($criteria["ecoboost"] == "true")) {
      $criteria["ecoboost"]  = true;
    }

    // echo "<pre>";
    // print_r($criteria);
    // echo "</pre>";
    // 
    // 
    
    // skip if search found
    // 
    if ($select['search']) {
      $criteria['model'] = $select['search']['model'];
    } else {
      foreach ($criteria as $key => $value) {
      if (($key != "year") && ($key != "price") && ($key != "mileage")) {
        // echo "$key\n";
      }



      if (is_string($value) ) {
        $vals = explode(";", $value);
        foreach ($vals as $val) {
         if (!isset($criteria['$or'])) {
          $criteria['$or'] = array(array("$key"=>array('$regex' => new MongoRegex("/(.*)?$val(.*)?/i"))));
        } else {
           array_push($criteria['$or'], array("$key"=>array('$regex' => new MongoRegex("/(.*)?$val(.*)?/i"))));
          }
        }

        if (isset($criteria['$or'])) {
          if (isset($criteria['$and'])) {
            array_push($criteria['$and'], array('$or'=>$criteria['$or']));
          } else {
            $criteria['$and'] = array(array('$or'=>$criteria['$or']));
          }
          unset($criteria['$or']);
        }
      } else {
        if (isset($criteria['$and'])) {
          array_push($criteria['$and'],  array($key=>$criteria[$key]));
        } else {
          $criteria['$and'] = array($key=>$criteria[$key]);
        }


      }
      unset($criteria[$key]);
    }
    }
    

    // echo "<pre>";
    // print_r($criteria);
    // echo "</pre>";

    // return;



		$offset= ($page-1)*$limit;


		

		if ($criteria) {
			$carCursor = $client->$db->$collection->find($criteria)->limit($limit)->skip($offset);
		} else {
			$carCursor = $client->$db->$collection->find()->limit($limit)->skip($offset);
		}


		$cars = array();
		foreach ($carCursor as $car) {
			if (isset($car["_id"]->{'$id'})) {
				$car["_id"] = $car["_id"]->{'$id'};
			}
			array_push($cars, $car);
		}
		return $cars;
	}




  // GetAllModelsByMake returns the list oof models based on a make
  public function GetAllModelsByMake($client,$db,$collection,$make) {



   
   $keys = array("make" => 1);
   $initial = array("models" => array());

   if (isset($_GET['new']) && ($_GET['new'] == "true")) {
    $condition = array('condition' => array("make" =>$make,"mileage" => array('$eq'=>0)));
   } else {
    $condition = array('condition' => array("make" =>$make,"mileage" => array('$gt'=>0)));
   }
   
   $reduce = "function (obj, prev) { prev.models.push(obj.model); }";
   $ret = $client->$db->$collection->group($keys, $initial, $reduce,$condition);

   return $ret;
  }

  public function GetMenuForFordModels($client,$db,$collection,$make) {
   $keys = array("make" => 1);
   $initial = array("models" => array());

   if (isset($_GET['new']) && ($_GET['new'] == "true")) {
    $condition = array('condition' => array("make" =>$make,"mileage" => array('$lte'=>5000)));
   } else {
    $condition = array('condition' => array("make" =>$make,"mileage" => array('$gt'=>5000)));
   }
   
   $reduce = "function (obj, prev) { prev.models.push(obj.model); }";
   $ret = $client->$db->$collection->group($keys, $initial, $reduce,$condition);

   return $ret;
  }


  // GetExteriorColors returns the list of each Exterior Color count
  public function GetExteriorColors($client,$db,$collection,$criteria) {
   
   $keys = array("exterior_color" => 1);
   $initial = array("count" => 0);
   $reduce = "function (obj, prev) { prev.count += 1; }";
   $condition = array('condition' => $criteria);
   $ret = $client->$db->$collection->group($keys, $initial, $reduce,$condition);

   return $ret;
  }

  // GetInteriorColors returns the list of each Interior Color count
  public function GetInteriorColors($client,$db,$collection,$criteria) {
   
   $keys = array("interior_color" => 1);
   $initial = array("count" => 0);
   $reduce = "function (obj, prev) { prev.count += 1; }";
   $condition = array('condition' => $criteria);
   $ret = $client->$db->$collection->group($keys, $initial, $reduce,$condition);

   return $ret;
  }

  // GetMakes returns the list of each Make count
  public function GetMakes($client,$db,$collection) {
   
   $keys = array("make" => 1);
   $initial = array("count" => 0);
   $reduce = "function (obj, prev) { prev.count += 1; }";
   $ret = $client->$db->$collection->group($keys, $initial, $reduce);

   return $ret;
  }

  // GetModels returns the list of each Model count
  public function GetModels($client,$db,$collection) {
   
   $keys = array("model" => 1);
   $initial = array("count" => 0);
   $reduce = "function (obj, prev) { prev.count += 1; }";
   $condition = array('condition' => array("make" => "Ford"));
   $ret = $client->$db->$collection->group($keys, $initial, $reduce,$condition);
   // echo "mne",
   return $ret;
  }

  // GetFuel returns the list of each Fuel count
  public function GetFuel($client,$db,$collection,$criteria) {
   
   $keys = array("fuel_type" => 1);
   $initial = array("count" => 0);
   $reduce = "function (obj, prev) { prev.count += 1; }";
   $condition = array('condition' => $criteria);
   $ret = $client->$db->$collection->group($keys, $initial, $reduce,$condition);

   return $ret;
  }

  // GetTrim returns the list of each Trim count
  public function GetTrim($client,$db,$collection,$criteria) {
   
   $keys = array("trim" => 1);
   $initial = array("count" => 0);
   $reduce = "function (obj, prev) { prev.count += 1; }";
   $condition = array('condition' => $criteria);
   $ret = $client->$db->$collection->group($keys, $initial, $reduce,$condition);

   return $ret;
  }

  // GetTransmissions returns the list of each Transmission count
  public function GetTransmissions($client,$db,$collection,$criteria) {
   
   $keys = array("transmission" => 1);
   $initial = array("count" => 0);
   $reduce = "function (obj, prev) { prev.count += 1; }";
   $condition = array('condition' => $criteria);
   $ret = $client->$db->$collection->group($keys, $initial, $reduce,$condition);

   return $ret;
  }

  // GetDriveTrain returns the list of each DriveTrain count
  public function GetDriveTrain($client,$db,$collection,$criteria) {
   
   $keys = array("drive_train" => 1);
   $initial = array("count" => 0);
   $reduce = "function (obj, prev) { prev.count += 1; }";
   $condition = array('condition' => $criteria);
   $ret = $client->$db->$collection->group($keys, $initial, $reduce,$condition);

   return $ret;
  }

  // GetEngine returns the list of each Engine count
  public function GetEngine($client,$db,$collection,$criteria) {
   
   $keys = array("engine_description" => 1);
   $initial = array("count" => 0);
   $reduce = "function (obj, prev) { prev.count += 1; }";
   $condition = array('condition' => $criteria);
   $ret = $client->$db->$collection->group($keys, $initial, $reduce,$condition);

   return $ret;
  }

  // GetDoor returns the list of each Door count
  public function GetDoor($client,$db,$collection,$criteria) {
   
   $keys = array("doors" => 1);
   $initial = array("count" => 0);
   $reduce = "function (obj, prev) { prev.count += 1; }";
   $condition = array('condition' => $criteria);
   $ret = $client->$db->$collection->group($keys, $initial, $reduce,$condition);

   return $ret;
  }

  // GetType returns the list of each New, Used or Certified count
  public function GetType($client,$db,$collection,$criteria) {
   
   

   $new_criteria = $criteria;
   $new_criteria["mileage"] = array('$lte' => 5000);
   $new = $client->$db->$collection->find($new_criteria)->count();

   $old_criteria = $criteria;
   $old_criteria["mileage"] = array('$gt' => 5000);
   $old = $client->$db->$collection->find($old_criteria)->count();

   $certified_criteria = $criteria;
   $certified_criteria["certified"] = true;
   $certified = $client->$db->$collection->find($certified_criteria)->count();

   $one_owner_criteria = $criteria;
   $one_owner_criteria["one_owner"] = true;
   $one_owner = $client->$db->$collection->find($one_owner_criteria)->count();

   $low_miles_criteria = $criteria;
   $low_miles_criteria["low_miles"] = true;
   $low_miles = $client->$db->$collection->find($low_miles_criteria)->count();

   $ecoboost_criteria = $criteria;
   $ecoboost_criteria["ecoboost"] = true;
   $ecoboost = $client->$db->$collection->find($ecoboost_criteria)->count();

   $ret = array();
   $ret["new"] = $new;
   $ret["used"] = $old;
   $ret["certified"] = $certified;
   $ret["one_owner"] = $one_owner;
   $ret["low_miles"] = $low_miles;
   $ret["ecoboost"] = $ecoboost;

   return $ret;
  }

  // GetListByIds returns the list of each ids count
  public function GetListByIds($client,$db,$collection,$ids) {
   
   $mIds = array();
   foreach (explode(",", $ids) as $id) {
      array_push($mIds, new MongoId($id));
    } 
   $cursor = $client->$db->$collection->find(array("_id"=>array('$in'=>$mIds)));

   $ret = array();
   foreach ($cursor as $vehicle) {
      $vehicle["_id"]= $vehicle["_id"]->{'$id'};
      $vehicle["id"] = $vehicle["_id"];
     array_push($ret, $vehicle);
   }

   return $ret;
  }



		
}

?>