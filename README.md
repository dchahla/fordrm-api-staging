**Table of Contents**  

- [FORDRM](#fordrm)
	- [API](#api)
		- [Vehicle structure](#vehicle-structure)
		- [GET A LIST OF CARS BY IDS](#get-a-list-of-cars-by-ids)
		- [GET A LIST OF CARS](#get-a-list-of-cars)
			- [Support multiple values](#support-multiple-values)
			- [Some examples:](#some-examples)
				- [1. Find 2 cars from 1988 to 2011](#1-find-2-cars-from-1988-to-2011)
				- [2. Find 2 cars whose price is from 30.000 to 40.000](#2-find-2-cars-whose-price-is-from-30000-to-40000)
				- [3. Find 2 cars whose mileage is under 60.000](#3-find-2-cars-whose-mileage-is-under-60000)
				- [4. Find 2 cars whose make is "Lincoln"](#4-find-2-cars-whose-make-is-lincoln)
				- [5. Find 2 cars whose model is "Focus"](#5-find-2-cars-whose-model-is-focus)
				- [6. Find 2 cars whose trim is "Hybrid"](#6-find-2-cars-whose-trim-is-hybrid)
				- [7. Find 2 cars whose trim is "Automatic"](#7-find-2-cars-whose-trim-is-automatic)
				- [8. Combination - Find all car having those following properties:](#8-combination---find-all-car-having-those-following-properties)
				- [9. Search for a list of vehicles based on predefined fields with the keyword ford](#9-search-for-a-list-of-vehicles-based-on-predefined-fields-with-the-keyword-ford)
		- [Get A Car](#get-a-car)
		- [Create A Car](#create-a-car)
		- [Delete A Car](#delete-a-car)
		- [Aggregation](#aggregation)
			- [Exterior color](#exterior-color)
			- [Interior color](#interior-color)
			- [Makes](#makes)
			- [Get Models By A Make](#get-models-by-a-make)
			- [Models](#models)
			- [Door types](#door-types)
			- [Fuel types](#fuel-types)
			- [Trim types](#trim-types)
			- [Transmission types](#transmission-types)
			- [Engine types](#engine-types)
			- [Drive trains types](#drive-trains-types)
			- [Get all type](#get-all-type)
			- [Get Sidebar](#get-sidebar)
		- [Get model list](#get-model-list)
		- [Get menu for Ford with 4 default models](#get-menu-for-ford-with-4-default-models)

#FORDRM

There are 1 collection in the databases: `vehicles`

##API

### Vehicle structure

|Property|Type|Description|
|--------|----|-----------|
|_id| MongoID | Id of a car in a collection
|dealer_id| Integer | A dealer ID
|stock_number| Integer | A stock number
|vin| String | A vin number
|type| String |
|certified| Boolean | Whether a vehicle has been certified
|year| Integer | A vehicle's year
|mileage| Integer | A vehicle's mileage
|make| String | A vehicle's make
|model| String | A vehicle's model
|model_code| String | A vehicle's model code
|series| String |
|trim| String | A vehicle' trim
|body_style| String | A vehicle's body style
|doors| Integer | The number of doors of a vehicle
|exterior_color| String | The exterior color of a vehicle
|interior_color| String | The interior color of a vehicle
|transmission| String | The transmission type of a vehicle
|drive_train| String |
|engine_description| String | An engine description of a vehicle
|fuel_type| String | A vehicle's fuel
|mpg_city| String |
|mpg_hwy| String |
|options| String | Options of a vehicles (comma separated values)
|package_code| String |
|dealer_comments| String |
|vehicle_comments| String |
|price| Integer | A vehicle's price
|msrp|  String |
|inventory_start_date| String | Start date of an inventory
|days_in_stock| Integer | The number of days in stock  of a vehicle
|image_urls| Array | A list of image urls
|images_modified_date| String | The modified date of images
|video_url| String | The video url of a vehicle
|dealer_name| String | The dealer name
|dealer_url| String | The dealer url
|low_miles| Boolean | Whether a vehicle has low miles. If [mileage]/[vehicle_age]<8000, low_miles = true
|one_owner| Boolean | Whether a vehicle has one owner. If mileage == 0 then one_owner=false else one_owner=true 


###GET A LIST OF CARS BY IDS

    GET /:db/:collection/list/:id1,:id2,:id3

Only support upto 80 cars  - 2000 characters maximum for each HTTP URL

For example

[http://fordrm.com/api/fordrm/vehicles/list/543c021f79e70e71d2975372,543c021f79e70e71d2975374](http://fordrm.com/api/fordrm/vehicles/list/543c021f79e70e71d2975372,543c021f79e70e71d2975374)

###GET A LIST OF CARS

	GET /:db/:collection/

For example

	GET http://fordrm.com/api/fordrm/vehicles/
	GET http://fordrm.com/api/fordrm/vehicles/?limit=1&page=1&filter[exterior_color]=White
	
Parameters

|Field|Type|Description|
|-----|-----|-----|
|limit| int| The number of returned cars|
|page| int| The page number|
|filter[$field] | string | search for a list based on a field


Sample response:

    [
      {
        "_id": "541b89b24099f76ea367e627",
        "dealer_id": 7069,
        "stock_number": "14T234",
        "vin": "1FTNE2EW4DDA31704",
        "type": "U",
        "certified": false,
        "year": 2013,
        "mileage": 6696,
        "make": "Ford",
        "model": "Econoline Cargo Van",
        "model_code": "W",
        "series": "E-250 Commercial",
        "trim": "Commercial",
        "body_style": "Full-size Cargo Van",
        "doors": 3,
        "exterior_color": "White",
        "interior_color": "Grey",
        "transmission": "4-Speed A/T",
        "drive_train": "RWD",
        "engine_description": "4.6L 8 Cyl.",
        "fuel_type": "Flexible",
        "mpg_city": 13,
        "mpg_hwy": 17,
        "options": "AM/FM Stereo,Wheels-Steel,Wheels-Wheel Covers,Remote Keyless Entry,Trip Odometer,Tilt Wheel,Traction     Control,Brakes-ABS-4 Wheel,4 Wheel Disc Brakes,Tire-Pressure Monitoring System,Cruise Control,Intermittent Wipers,    Running Boards,Bucket Seats,Cloth Seats,Power Mirrors,Mirrors-Folding,Air Bag - Driver,Air Bag - Passenger,Air     Conditioning,Tachometer,Tinted Glass,Reading Lamps-Front,Power Steering,Power Windows,Power Window - Driver One     Touch Down,Power Door Locks,Additional Power Outlet(s),Cup Holders",
        "package_code": "",
        "dealer_comments": "",
        "vehicle_comments": "THIS CARGO IS AN XLT WITH THE 4.6 LTR V8, ALL THE POWER OPTIONS, CLOTH SEATS, AND THE BULK     HEAD!!! A GREAT VALUE WITH ONLY 6K FOR MILES!!!",
        "price": 22987,
        "msrp": "",
        "inventory_start_date": "06/19/2014",
        "days_in_stock": 89,
        "image_urls": [
          "http://i.autouplinktech.com/t640/a0fb5fde46fd46678c3667754c97f410.jpg",
          "http://i.autouplinktech.com/t640/43d8697a7f704502a1d7a0ab6b90d2f3.jpg",
          "http://i.autouplinktech.com/t640/f8053f59fd664a19ba9d1ff6363a137b.jpg",
          "http://i.autouplinktech.com/t640/36e68d32644844faafb0b480587fa435.jpg",
          "http://i.autouplinktech.com/t640/f626e317c5ad4ec6aa1237422d2b6b05.jpg",
          "http://i.autouplinktech.com/t640/e9eb0f2faab2456dabde6f1303db10b2.jpg",
          "http://i.autouplinktech.com/t640/048f8dcb948d4cbab6e0dad763106d10.jpg",
          "http://i.autouplinktech.com/t640/ca84d9bab2054bb7b492827a0d07a1e2.jpg",
          "http://i.autouplinktech.com/t640/ccee49f8ee8c4d978533158ca3230297.jpg",
          "http://i.autouplinktech.com/t640/07033158042c43c0afe5cbc37bd1b3d2.jpg",
          "http://i.autouplinktech.com/c640/ad_94.jpg",
          "http://i.autouplinktech.com/c640/ad_95.jpg",
          "http://i.autouplinktech.com/c640/ad_373.jpg"
        ],
        "images_modified_date": "08/20/2014 16:00",
        "video_url": "",
        "dealer_name": "Roseville Midway Ford",
        "dealer_url": "www.rosevillemidwayford.com",
        "low_miles": true,
        "one_owner": true
      }
    ]


#### Support multiple values


|Property|Value|Equivalent query parameter|
|--------|-----|--------------------------|
|Make | Ford;WG | filter[make]=Ford;WG
|Model | Escape;fusion | filter[model]=Escape;fusion
|Exterior Color | gray;red | filter[exterior_color]=gray;red
|year| from 1988 to 2013| filter[year][$gte]=1998&filter[year][$lte]=2013|



http://fordrm.com/api/fordrm/vehicles/?filter[make]=Ford;WG&filter[model]=Escape;fusion&filter[exterior_color]=gray;red&filter[fuel_type]=Gas;flexible&filter[year][$gte]=1998&filter[year][$lte]=2013



#### Some examples:

Several operator insipired by Mongo:

|Operator|Description|
|--------|-----------|
|$gt| greater than
|$lt| less than
|$gte| greater than or equal to
|$lte| less than or equal to


##### 1. Find 2 cars from 1988 to 2011

|Property|Value|Equivalent query parameter|
|--------|-----|--------------------------|
|limit| 2| limit=2|
|year| from 1988 to 2011| filter[year][$gte]=1998&filter[year][$lte]=2011|

[http://fordrm.com/api/fordrm/vehicles/?limit=2&filter[year][$gte]=1998&filter[year][$lte]=2011](http://fordrm.com/api/fordrm/vehicles/?limit=2&filter[year][$gte]=1998&filter[year][$lte]=2011)

##### 2. Find 2 cars whose price is from 30.000 to 40.000

|Property|Value|Equivalent query parameter|
|--------|-----|--------------------------|
|limit| 2| limit=2|
|price| from 30.000 to 40.000| filter[price][$gte]=30000&filter[price][$lte]=40000|

[http://fordrm.com/api/fordrm/vehicles/?limit=2&filter[price][$gte]=30000&filter[price][$lte]=40000](http://fordrm.com/api/fordrm/vehicles/?limit=2&filter[price][$gte]=30000&filter[price][$lte]=40000)

##### 3. Find 2 cars whose mileage is under 60.000

|Property|Value|Equivalent query parameter|
|--------|-----|--------------------------|
|limit| 2| limit=2|
|mileage| under 60.000| filter[mileage][$lte]=60000|

[http://fordrm.com/api/fordrm/vehicles/?limit=2&filter[mileage][$lte]=60000](http://fordrm.com/api/fordrm/vehicles/?limit=2&filter[mileage][$lte]=60000)


##### 4. Find 2 cars whose make is "Lincoln"

|Property|Value|Equivalent query parameter|
|--------|-----|--------------------------|
|limit| 2| limit=2|
|make| Lincoln| filter[make]=Lincoln|

[http://fordrm.com/api/fordrm/vehicles/?limit=2&filter[make]=Lincoln](http://fordrm.com/api/fordrm/vehicles/?limit=2&filter[make]=Lincoln)


##### 5. Find 2 cars whose model is "Focus"

|Property|Value|Equivalent query parameter|
|--------|-----|--------------------------|
|limit| 2| limit=2|
|model| Focus| filter[model]=Focus|

[http://fordrm.com/api/fordrm/vehicles/?limit=2&filter[model]=Focus](http://fordrm.com/api/fordrm/vehicles/?limit=2&filter[model]=Focus)

##### 6. Find 2 cars whose trim is "Hybrid"

|Property|Value|Equivalent query parameter|
|--------|-----|--------------------------|
|limit| 2| limit=2|
|trim| Hybrid| filter[trim]=Hybrid|

[http://fordrm.com/api/fordrm/vehicles/?limit=2&filter[trim]=Hybrid](http://fordrm.com/api/fordrm/vehicles/?limit=2&filter[trim]=Hybrid)

##### 7. Find 2 cars whose trim is "Automatic"

|Property|Value|Equivalent query parameter|
|--------|-----|--------------------------|
|limit| 2| limit=2|
|transmission| Automatic| filter[transmission]=Automatic|

[http://fordrm.com/api/fordrm/vehicles/?limit=2&filter[transmission]=Automatic](http://fordrm.com/api/fordrm/vehicles/?limit=2&filter[transmission]=Automatic)

##### 8. Combination - Find all car having those following properties:

|Property|Value|Equivalent query parameter|
|--------|-----|--------------------------|
|Make | Ford | filter[make]=Ford
|Model | Escape | filter[model]=Escape
|Mileage | from 35.000 to 40.000 | filter[price][$gte]=35000&filter[price][$lte]=40000
|Price |  from 15.000 to 20.000 | filter[price][$gte]=15000&filter[price][$lte]=20000
|Year | from 2010 to 2013 | filter[year][$gte]=2010&filter[year][$lte]=2013
|Doors | 4 | filter[doors]=4
|Fuel Type | Gas | filter[fuel_type]=Gas
|Exterior color | BLACK | filter[exterior_color]=BLACK

[http://fordrm.com/api/fordrm/vehicles/?filter[make]=Ford&filter[model]=Escape&filter[price][$gte]=35000&filter[price][$lte]=40000&filter[price][$gte]=15000&filter[price][$lte]=20000&filter[year][$gte]=2010&filter[year][$lte]=2013&filter[doors]=4&filter[fuel_type]=Gas&filter[exterior_color]=BLACK](http://fordrm.com/api/fordrm/vehicles/?filter[make]=Ford&filter[model]=Escape&filter[price][$gte]=35000&filter[price][$lte]=40000&filter[price][$gte]=15000&filter[price][$lte]=20000&filter[year][$gte]=2010&filter[year][$lte]=2013&filter[doors]=4&filter[fuel_type]=Gas&filter[exterior_color]=BLACK)



##### 9. Search for a list of vehicles based on predefined fields with the keyword ford


|Property|Value|Equivalent query parameter|
|--------|-----|--------------------------|
|search[make]| ford | filter[make]=ford
|search[model]| ford | search[model]=ford
|search[options] | ford | search[options]=ford


http://fordrm.com/api/fordrm/vehicles/?search[make]=ford&search[model]=ford&search[options]=ford



###Get A Car


	GET /:db/:collection/:id

For example

	GET http://fordrm.com/api/fordrm/vehicles/541b89b24099f76ea367e640
	
Response
	
    {
      "_id": "541b89b24099f76ea367e640",
      "dealer_id": 7069,
      "stock_number": "157022",
      "vin": "1FTNR2CM7FKA01990",
      "type": "N",
      "certified": false,
      "year": 2015,
      "mileage": 4,
      "make": "Ford",
      "model": "Transit Cargo Van",
      "model_code": "",
      "series": "T-250 148 Med Rf 9000 GVWR Sliding RH Dr",
      "trim": "",
      "body_style": "Full-size Cargo Van",
      "doors": 3,
      "exterior_color": "OXFORD WHITE",
      "interior_color": "CLOTH PEWTER",
      "transmission": "",
      "drive_train": "RWD",
      "engine_description": "3.7L V6",
      "fuel_type": "Gas",
      "mpg_city": 0,
      "mpg_hwy": 0,
      "options": "AM/FM Stereo,Wheels-Steel,Remote Keyless Entry,Tilt Wheel,Traction Control,Brakes-ABS-4 Wheel,4 Wheel     Disc Brakes,Tire-Pressure Monitoring System,Intermittent Wipers,Wipers-Variable Speed Intermittent,Bucket Seats,Vinyl     Seats,Mirrors-Pwr Driver,Mirrors-Vanity-Driver,Mirrors-Vanity-Passenger,Air Bag - Driver,Air Bag - Passenger,Air Bag     On/Off Switch - Passenger,Door-Passenger 3rd,Air Conditioning,Power Steering,Power Windows,Power Door Locks",
      "package_code": "",
      "dealer_comments": "",
      "vehicle_comments": "",
      "price": 0,
      "msrp": "",
      "inventory_start_date": "07/03/2014",
      "days_in_stock": 75,
      "image_urls": [
        "http://i.autouplinktech.com/t640/e3bdaca13ed14b089bd40d20edec8edc.jpg",
        "http://i.autouplinktech.com/t640/c660a108e4294c79ae815eec34cc1d41.jpg",
        "http://i.autouplinktech.com/t640/69a1e18b5af340f09ced618c1eaf2379.jpg",
        "http://i.autouplinktech.com/t640/cdcca77fc8e945bf895b9e6d768ce4cd.jpg",
        "http://i.autouplinktech.com/t640/17fa830baf424362ac55edf8e298dff8.jpg",
        "http://i.autouplinktech.com/t640/e17533875f59431abe9abfc5e45e1523.jpg",
        "http://i.autouplinktech.com/t640/e3c1b0680a2042129be50ac66fc5a409.jpg",
        "http://i.autouplinktech.com/t640/a53fc7ffe4ea41788efc697b4267ca38.jpg",
        "http://i.autouplinktech.com/t640/340d431d045f4dc1a948dc51a50a3bd2.jpg",
        "http://i.autouplinktech.com/t640/78c251cb6cdc4a2eb00ff20a10e49cd5.jpg",
        "http://i.autouplinktech.com/c640/ad_94.jpg",
        "http://i.autouplinktech.com/c640/ad_95.jpg",
        "http://i.autouplinktech.com/c640/ad_373.jpg"
      ],
      "images_modified_date": "08/20/2014 16:00",
      "video_url": "",
      "dealer_name": "Roseville Midway Ford",
      "dealer_url": "www.rosevillemidwayford.com",
      "low_miles": true,
      "one_owner": true
    }

### Create A Car


	POST /:db/:collection/

For example


	POST http://fordrm.com/api/fordrm/vehicles/

	
**POST CONTENT**

    {
        "dealer_id": 7069,
        "stock_number": "157022",
        "vin": "1FTNR2CM7FKA01990",
        "type": "N",
        "certified": false,
        "year": 2015,
        "mileage": 4,
        "make": "Ford",
        "model": "Transit Cargo Van",
        "model_code": "",
        "series": "T-250 148 Med Rf 9000 GVWR Sliding RH Dr",
        "trim": "",
        "body_style": "Full-size Cargo Van",
        "doors": 3,
        "exterior_color": "OXFORD WHITE",
        "interior_color": "CLOTH PEWTER",
        "transmission": "",
        "drive_train": "RWD",
        "engine_description": "3.7L V6",
        "fuel_type": "Gas",
        "mpg_city": 0,
        "mpg_hwy": 0,
        "options": "option 1, option 2",
        "package_code": "",
        "dealer_comments": "",
        "vehicle_comments": "",
        "price": 0,
        "msrp": "",
        "inventory_start_date": "07/03/2014",
        "days_in_stock": 75,
        "image_urls": [
            "http://i.autouplinktech.com/t640/e3bdaca13ed14b089bd40d20edec8edc.jpg",
            "http://i.autouplinktech.com/t640/c660a108e4294c79ae815eec34cc1d41.jpg"
        ],
        "images_modified_date": "08/20/2014 16:00",
        "video_url": "",
        "dealer_name": "Roseville Midway Ford",
        "dealer_url": "www.rosevillemidwayford.com",
        "low_miles": true,
        "one_owner": true,
        "_id": "541befde9934ff6ccc0041a7"
    }


Response:

    {
        "source_dealer_id": 11111,
        "srouce_dealer_name": "Freeway Ford",
        "address1": "9700 Lyndale Ave S",
        "city": "Bloomington",
        "state": "MN",
        "zip": 55420,
        "phone_number": "(952) 888-9481",
        "vin": "1FTNE1EL4DDA90266",
        "sale_price": 31635,
        "miles": 18,
        "exterior_color": "White",
        "interior_color": "CE CLOTH BUCKET SEAT",
        "description": "\"Prices reflect all factory incentives may include Ford Credit Financing or trade-in   assistance bonus cash. - - - 2013 Ford Econoline Cargo Van E-150 - - - AM/FM Stereo,Wheels-Steel,Wheels   -Wheel Covers,Tilt Wheel,Traction Control,Brakes-ABS-4 Wheel,4 Wheel Disc Brakes,Tire-Pressure     Monitoring System,Intermittent Wipers,Bucket Seats,Air Bag - Driver,Air Bag - Passenger,Air     Conditioning,Power Steering,Additional Power Outlet(s)\"",
        "image_url": "\"http://i.autouplinktech.com/t640/6A7ACFA6A2014B20A405ED4FD176F0D3.jpg?mod=201308280000, http://i.autouplinktech.com/t640/0ECBE41BCC024D2AB6F33DAAB71AD126.jpg?mod=201308280000,http://i. autouplinktech.com/t640/9FCE0DB73B8C47379DEDAF97C8ED291E.jpg?mod=201308280000,http://i.autouplinktech.   com/t640/E3B8CBB85F9043DF9BCD1748ACB59632.jpg?mod=201308280000,http://i.autouplinktech.com/    t640/763A2B6E20CA4F409227F68E1C26DBD0.jpg?mod=201308280000,http://i.autouplinktech.com/c640/ad_445. jpg?mod=201308280000\"",
        "car_type": "N",
        "engine_type": "",
        "fuel_type": "",
        "door": 3,
        "stock_id": 131541,
        "msrp": 31635,
        "transmission": "Automatic",
        "trim": "",
        "options": "\"AM/FM Stereo,Wheels-Steel,Wheels-Wheel Covers,Tilt Wheel,Traction Control,Brakes-ABS-4    Wheel,4 Wheel Disc Brakes,Tire-Pressure Monitoring System,Intermittent Wipers,Bucket Seats,Air Bag -   Driver,Air Bag - Passenger,Air Conditioning,Power Steering,Additional Power Outlet(s)\"",
        "_id": "5400b47d9934ff05220041a7"
    }

###Delete A Car

	DELETE /:db/:collection/:id
	
For example:

	DELETE http://fordrm.com/api/fordrm/vehicles/5400b47d9934ff05220041a7
	
Response:


	{
   	 "success": "deleted"
	}


### Aggregation

#### Exterior color

Get all exterior color and their count

    http://fordrm.com/api/fordrm/vehicles/agg/exterior_color


Sample response

    {
      "exterior_colors": {
        "Beige": 1,
        "Black": 56,
        "Blue": 32,
        "Brown": 3,
        "Gold": 1,
        "Gray": 25,
        "Green": 8,
        "Red": 49,
        "Silver": 27,
        "White": 72,
        "Yellow": 1
      }
    }


Add filter

    http://fordrm.com/api/fordrm/vehicles/agg/exterior_color?filter[make]=Ford&filter[model]=Focus

Response

    {
      "exterior_colors": {
        "Black": 6,
        "Blue": 2,
        "Gray": 5,
        "Red": 6,
        "Silver": 3
      }
    }


#### Interior color

Get all interior color and their count

    http://fordrm.com/api/fordrm/vehicles/agg/interior_color

Sample response

    {
      "interior_colors": {
        "Black": 101,
        "Charcoal": 80,
        "Gray": 75,
        "Red": 1,
        "Silver": 2,
        "Tan": 11
      }
    }

Add fielter

    http://fordrm.com/api/fordrm/vehicles/agg/interior_color?filter[make]=Ford&filter[model]=Focus


Response

    {
      "interior_colors": {
        "Black": 13,
        "Charcoal": 13,
        "Gray": 1,
        "Tan": 6
      }
    }

#### Makes

Get all make and their count

    http://fordrm.com/api/fordrm/vehicles/agg/makes

Sample response:

    {
      "makes": {
        "Ford": 316,
        "WG": 1,
        "Saturn": 1,
        "Chrysler": 1,
        "Lincoln": 3,
        "Chevrolet": 6,
        "Dodge": 1,
        "GMC": 1,
        "Pontiac": 2,
        "Hyundai": 1
      }
    }




#### Get Models By A Make

Syntax

    http://fordrm.com/api/fordrm/vehicles/models/:make?new=<true/false>


The params


|Param|Description|
|-----|-----|
|new | There are 2 values: `true` or `false`. It indicates whether the cars is new or used


Example

[http://fordrm.com/api/fordrm/vehicles/models/Ford](http://fordrm.com/api/fordrm/vehicles/models/Ford)

[http://fordrm.com/api/fordrm/vehicles/models/Chevrolet](http://fordrm.com/api/fordrm/vehicles/models/Chevrolet)

[http://fordrm.com/api/fordrm/vehicles/models/Lincoln](http://fordrm.com/api/fordrm/vehicles/models/Lincoln)

[http://fordrm.com/apifordrm/vehicles/models/Ford?new=false](http://fordrm.com/apifordrm/vehicles/models/Ford?new=false)

Sample return for Chevrolet


    {
      "make": "Chevrolet",
      "models": {
        "Cobalt": 1,
        "Malibu": 1,
        "Impala": 1,
        "TrailBlazer": 1,
        "Monte Carlo": 1,
        "Suburban": 1
      }
    }


#### Models

Get all models and their count

    http://fordrm.com/api/fordrm/vehicles/agg/models

Sample response:

    {
      "models": {
        "200": 1,
        "Super Duty F-250 SRW": 6,
        "Escape": 37,
        "TANK": 1,
        "F-150": 107,
        "Transit Connect Wagon": 8,
        "Econoline Cargo Van": 4,
        "Flex": 4,
        "Super Duty F-350 SRW": 15,
        "Super Duty F-550 DRW": 4,
        "Aura": 1,
        "Taurus": 10,
        "Focus": 30,
        "Fiesta": 8,
        "C-Max Hybrid": 5,
        "Transit Cargo Van": 8,
        "Mustang": 4,
        "Edge": 3,
        "Explorer": 9,
        "MKS": 1,
        "Fusion": 34,
        "Expedition EL": 4,
        "C-Max Energi": 1,
        "Fusion Energi": 1,
        "Super Duty F-350 DRW": 1,
        "F350 SUPER CAB 4X4 DALLY": 1,
        "Transit Connect": 7,
        "Suburban": 1,
        "Ram 2500": 1,
        "Ranger": 1,
        "Excursion": 1,
        "MKZ": 1,
        "Econoline Commercial Cutaway": 2,
        "Envoy XL": 1,
        "Super Duty F-450 DRW": 1,
        "Grand Prix": 1,
        "MKX": 1,
        "Elantra": 1,
        "G6": 1,
        "Monte Carlo": 1,
        "TrailBlazer": 1,
        "Impala": 1,
        "Cobalt": 1,
        "Malibu": 1
      }
    }

#### Door types

Get all doors

    http://fordrm.com/api/fordrm/vehicles/agg/door

Sample response:

    {
      "doors": {
        "2": 27,
        "3": 13,
        "4": 293
      }
    }

Add filter

    http://fordrm.com/api/fordrm/vehicles/agg/door?filter[make]=Ford&filter[model]=Focus

Response:

    {
      "doors": {
        "4": 30
      }
    }

#### Fuel types

Get all Fuel types and their count

    http://fordrm.com/api/fordrm/vehicles/agg/fuel

Sample response:

    {
      "fuel": {
        "Flexible": 104,
        "Gas": 172,
        "Diesel": 23,
        "Hybrid Fuel": 8,
        "Plug-In/Gas": 1
      }
    }

Add filter

    http://fordrm.com/api/fordrm/vehicles/agg/fuel?filter[make]=Ford&filter[model]=Focus

Response:

    {
      "fuel": {
        "Flexible": 26,
        "Gas": 2
      }
    }

#### Trim types

Get all trim types and their count

    http://fordrm.com/api/fordrm/vehicles/agg/trim

Sample response:

    {
      "trims": {
        "XLT": 91,
        "SE": 65,
        "SEL": 30,
        "Lariat": 18,
        "Limited": 15,
        "STX": 14,
        "XL": 13,
        "Titanium": 11,
        "S": 5,
        "FX4": 4,
        "Commercial": 4,
        "SVT Raptor": 3,
        "Sport": 2,
        "LT w/3LT": 2,
        "GLS": 1,
        "1SV Value Leader": 1,
        "SS Supercharged": 1,
        "SS": 1,
        "LT w/1LT": 1,
        "LT": 1,
        "ZX5 Base": 1,
        "GT Premium": 1,
        "SES": 1,
        "Hybrid": 1,
        "SE Luxury": 1,
        "Platinum": 1,
        "XR": 1,
        "King Ranch": 1,
        "SE Hybrid": 1,
        "Limited w/EcoBoost": 1,
        "SHO": 1,
        "SLT": 1,
        "S Hybrid": 1
      }
    }


Add filter

    http://fordrm.com/api/fordrm/vehicles/agg/trim?filter[make]=Ford&filter[model]=Focus


Response

    {
      "trims": {
        "SE": 23,
        "SEL": 3,
        "Titanium": 1,
        "S": 1,
        "ZX5 Base": 1
      }
    }

#### Transmission types

Get all transmission types and their count

|Field|Description|
|-----|--------|
|A/T;Automatic;Continuously |Automatic
|Manual | Manual|


    http://fordrm.com/api/fordrm/vehicles/agg/transmission

Sample response:

    {
      "transmissions": {
        "A/T;Automatic;Continuously": 94,
        "Manual": 4
      }
    }

Add filter

    http://fordrm.com/api/fordrm/vehicles/agg/transmission?filter[make]=Ford&filter[model]=Focus

Response

    {
      "transmissions": {
        "A/T;Automatic;Continuously": 6
      }
    }


#### Engine types

Get all engines types and their count

    http://fordrm.com/api/fordrm/vehicles/agg/engine

Sample response:

    {
      "engines": {
        "4 Cyl.": 127,
        "5 Cyl.": 1,
        "8 Cyl.": 99
      }
    }


OLD response

    {
      "engines": {
        "6.2L 8 Cyl.": 6,
        "2.5L 4 Cyl.": 33,
        " ": 8,
        "5.0L 8 Cyl.": 58,
        "3.5L V6": 60,
        "4.6L 8 Cyl.": 6,
        "6.0L 8 Cyl.": 5,
        "3.6L V6": 2,
        "2.0L 4 Cyl.": 62,
        "1.6L 4 Cyl.": 19,
        "3.7L V6": 9,
        "6.7L 8 Cyl.": 15,
        " 8": 7,
        "1.5L 4 Cyl.": 8,
        "6.0L 8": 1,
        "5.4L 8 Cyl.": 5,
        "3.0L V6": 8,
        "5.3L 8 Cyl.": 2,
        " 6": 6,
        "2.3L 4 Cyl.": 2,
        "5.7L 8 Cyl.": 1,
        " 4": 2,
        "3.1L V6": 1,
        "6.4L 8 Cyl.": 1,
        "3.2L 5 Cyl.": 1,
        "2.4L 4 Cyl.": 2,
        "3.8L V6": 1,
        "4.2L I6": 1,
        "2.2L 4 Cyl.": 1
      }
    }

Add filter

    http://fordrm.com/api/fordrm/vehicles/agg/engine?filter[make]=Ford&filter[model]=Focus

Response:

    {
      "engines": {
        "4 Cyl.": 29
      }
    }    

#### Drive trains types

Get all drive_train types and their count

`Drive train`==> `Drive type`

|Field|Translation|
|-----|-----------|
|4WD;4WD:DRW;AWD|4WD/AWD|
|FWD|Front Wheel Drive|
|RWD|Rear Wheel Drive|


    http://fordrm.com/api/fordrm/vehicles/agg/drive_train

Sample response:

    {
      "drive_trains": {
        "4WD;4WD:DRW;AWD": 174,
        "FWD": 118,
        "RWD": 19
      }
    }


Add filter

    http://fordrm.com/api/fordrm/vehicles/agg/drive_train?filter[make]=Ford&filter[model]=Focus

Response:

    {
      "drive_trains": {
        "FWD": 29
      }
    }

#### Get all type

This is special: type has constant 5 options: `new`, `old`, `certified`, `one_owner` and `low_miles`

Each option is corresponent to a field in the vehicle document.

So each has to be queried differently.

- If `new` is checked ==> the param for query should be `&filter[mileage][$lte]=5000`

EX: Query for `new Ford`

http://fordrm.com/api/fordrm/vehicles/?filter[make]=Ford&limit=100&filter[mileage][$lte]=5000

- If `old` is checked ==> the param for query should be `&filter[mileage][$gt]=5000`

EX: Query for `old Ford`

http://fordrm.com/api/fordrm/vehicles/?filter[make]=Ford&limit=400&filter[mileage][$gt]=5000

- If `certified` is checked ==> the param for query should be `&filter[certified]=true`

Ex: Query for `certified Ford`

http://fordrm.com/api/fordrm/vehicles/?filter[make]=Ford&limit=400&filter[certified]=true

- If `one_owner` is checked ==> the param for query should be `&filter[one_owner]=true`

Ex: Query for `one_owner for Ford`

http://fordrm.com/api/fordrm/vehicles/?filter[make]=Ford&limit=400&filter[one_owner]=true

- If `low_miles` is checked ==> the param for query should be `&filter[low_miles]=true`

Ex: Query for `low_miles for Ford`

http://fordrm.com/api/fordrm/vehicles/?filter[make]=Ford&limit=400&filter[low_miles]=true


    http://fordrm.com/api/fordrm/vehicles/agg/type

Response:

    {
      "types": {
        "new": 4,
        "old": 330,
        "certified": 9,
        "one_owner": 330,
        "low_miles": 245
      }
    }

Add filter

    http://fordrm.com/api/fordrm/vehicles/agg/type?filter[make]=Ford&filter[model]=Focus

Response

    {
      "types": {
        "new": 0,
        "old": 30,
        "certified": 1,
        "one_owner": 30,
        "low_miles": 24
      }
    }


#### Get Sidebar

    http://fordrm.com/api/fordrm/vehicles/agg/sidebar


Sample response

    {
      "exterior_color": {
        "Beige": 1,
        "Black": 67,
        "Blue": 35,
        "Brown": 3,
        "Gold": 3,
        "Gray": 28,
        "Green": 7,
        "Red": 49,
        "Silver": 31,
        "White": 66
      },
      "interior_color": {
        "Black": 125,
        "Charcoal": 100,
        "Gray": 63,
        "Red": 1,
        "Silver": 5,
        "Tan": 19
      },
      "fuel_type": {
        "Flexible": 106,
        "Gas": 192,
        "Diesel": 17,
        "Hybrid Fuel": 8
      },
      "trim": {
        "XLT": 92,
        "SE": 81,
        "SEL": 25,
        "Limited": 19,
        "Lariat": 19,
        "Titanium": 16,
        "STX": 12,
        "S": 10,
        "XL": 6,
        "Commercial": 4,
        "FX4": 4,
        "Sport": 2,
        "SVT Raptor": 2,
        "LT w/3LT": 2,
        "SXT": 2,
        "Limited w/EcoBoost": 2,
        "LT w/1LT": 2,
        "GT": 1,
        "ST": 1,
        "GT Premium": 1,
        "Premium": 1,
        "SES": 1,
        "SS Supercharged": 1,
        "SS": 1,
        "SHO": 1,
        "Hybrid": 1,
        "SE Hybrid": 1,
        "Platinum": 1,
        "SE Luxury": 1,
        "S Hybrid": 1,
        "LT": 1,
        "ZX5 Comfort": 1,
        "Mainstreet": 1
      },
      "transmission": {
        "A/T;Automatic;Continuously": 109,
        "Manual": 4
      },
      "engine_description": {
        "3 Cyl.": 1,
        "4 Cyl.": 144,
        "5 Cyl.": 1,
        "8 Cyl.": 93
      },
      "drive_train": {
        "4WD;4WD:DRW;AWD": 172,
        "FWD": 137,
        "RWD": 15
      },
      "doors": {
        "2": 25,
        "3": 11,
        "4": 307
      },
      "type": {
        "new": 5,
        "used": 338,
        "certified": 10,
        "one_owner": 27,
        "low_miles": 254,
        "ecoboost": 19
      }
    }

Add filter

http://fordrm.com/api/fordrm/vehicles/agg/sidebar?filter[make]=Ford&filter[model]=Focus

Response:

    {
      "exterior_color": {
        "Black": 10,
        "Blue": 2,
        "Gray": 7,
        "Red": 9,
        "Silver": 5
      },
      "interior_color": {
        "Black": 21,
        "Charcoal": 21,
        "Gray": 1,
        "Tan": 12
      },
      "fuel_type": {
        "Gas": 2,
        "Flexible": 35
      },
      "trim": {
        "SE": 33,
        "SEL": 2,
        "Titanium": 1,
        "ZX5 Comfort": 1,
        "S": 1
      },
      "transmission": {
        "A/T;Automatic;Continuously": 11
      },
      "engine_description": {
        "4 Cyl.": 38
      },
      "drive_train": {
        "FWD": 38
      },
      "doors": {
        "4": 38
      },
      "type": {
        "new": 0,
        "used": 38,
        "certified": 2,
        "one_owner": 1,
        "low_miles": 30,
        "ecoboost": 0
      }
    }


***

### Get model list

**Updated on Dec 03** Only show models of Ford

    GET http://fordrm.com/api/fordrm/vehicles/models

Get a list of models

```json
[
  "Fusion Energi",
  "F-150",
  "Focus",
  "Fiesta",
  "Super Duty F-550 DRW",
  "F350 SUPER CAB 4X4 DALLY",
  "Escape",
  "Expedition EL",
  "Explorer",
  "Fusion",
  "Super Duty F-350 SRW",
  "Transit Connect",
  "Super Duty F-250 SRW",
  "Taurus",
  "Edge",
  "Transit Connect Wagon",
  "Econoline Cargo Van",
  "Flex",
  "C-Max Hybrid",
  "Transit Cargo Van",
  "Mustang",
  "Ranger",
  "F-250 XLT",
  "Thunderbird",
  "Five Hundred",
  "Econoline Wagon",
  "Expedition"
]
```

***

### Get menu for Ford with 4 default models

    GET http://fordrm.com/api/fordrm/vehicles/menu?new=<true/false>

We only display 4 models: F-150, Fusion, Focus, Escape


Params:

|Param|Type|Description|
|------|------|---------|
|new | boolean | Indicate whether we want to show agg result for new/old vehicles|


For example:

    GET http://fordrm.com/api/fordrm/vehicles/menu?new=false


Response:

```json
{
  "make": "Ford",
  "models": {
    "F-150": 99,
    "Fusion": 40,
    "Focus": 38,
    "Escape": 39
  }
}
```

