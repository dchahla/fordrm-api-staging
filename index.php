<?php
// ini_set('display_errors',1);
// ini_set('display_startup_errors',1);
// error_reporting(E_ALL);

// Allow cross-domain access
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true"); 
header('Access-Control-Allow-Headers: X-Requested-With');
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, OPTIONS');
header('Access-Control-Max-Age: 86400'); 

require 'Slim/Slim/Slim.php';
require 'lib/api.php';
define('MONGO_HOST', 'localhost');

$app = new Slim();

// AGGREGATION
$app->get(	'/:db/:collection/agg/exterior_color',		'_getExteriorColor');
$app->get(	'/:db/:collection/agg/interior_color',		'_getInteriorColor');
$app->get(	'/:db/:collection/agg/makes',		'_getMakes');
$app->get(	'/:db/:collection/agg/models',		'_getModels');
$app->get(	'/:db/:collection/agg/fuel',		'_getFuel');
$app->get(	'/:db/:collection/agg/trim',		'_getTrim');
$app->get(	'/:db/:collection/agg/transmission',		'_getTransmissions');
$app->get(	'/:db/:collection/agg/drive_train',		'_getDriveTrain');
$app->get(	'/:db/:collection/agg/engine',		'_getEngine');
$app->get(	'/:db/:collection/agg/door',		'_getDoor');
$app->get(	'/:db/:collection/agg/type',		'_getType');

// only show models of Ford
$app->get(	'/:db/:collection/models',		'_getModelList');

$app->get(	'/:db/:collection/agg/sidebar',		'_getSidebar');

$app->get(	'/:db/:collection/models/:make',		'_getAllModelsByMake');

$app->get(	'/:db/:collection/menu',		'_getMenuForFordModels');

$app->get(	'/:db/:collection/list/:ids',		'_listGetList');

$app->get(	'/:db/:collection/',		'_list');
$app->post(	'/:db/:collection/',		'_create');
$app->delete(	'/:db/:collection/:id',		'_delete');
$app->get(	'/:db/:collection/:id',		'_get');




function _list($db,$collection){
	// print_r($_GET);
	APICar::GetCarList($db,$collection);
}

function _create($db,$collection){
	APICar::CreateNewCar($db,$collection);
}

function _delete($db,$collection,$id){
	APICar::DeleteCar($db,$collection,$id);
}

function _get($db,$collection,$id){
	APICar::GetACar($db,$collection,$id);
}

function _getExteriorColor($db,$collection){
	echo json_encode(APICar::GetExteriorColors($db,$collection)) ;
}

function _getInteriorColor($db,$collection){
	echo json_encode(APICar::GetInteriorColors($db,$collection)) ;
}

function _getMakes($db,$collection){
	APICar::GetMakes($db,$collection);
}

function _getModels($db,$collection){
	APICar::GetModels($db,$collection);
}

function _getFuel($db,$collection){
	echo json_encode(APICar::GetFuel($db,$collection));
}

function _getTrim($db,$collection){
	echo json_encode(APICar::GetTrim($db,$collection));
}

function _getTransmissions($db,$collection){
	echo json_encode(APICar::GetTransmissions($db,$collection));
}

function _getDriveTrain($db,$collection){
	echo json_encode(APICar::GetDriveTrain($db,$collection));
}

function _getEngine($db,$collection){
	echo json_encode(APICar::GetEngine($db,$collection));
}

function _getDoor($db,$collection){
	echo json_encode(APICar::GetDoor($db,$collection));
}

function _getType($db,$collection){
	echo json_encode(APICar::GetType($db,$collection));
}


function _getModelList($db,$collection){
	APICar::GetModelList($db,$collection);
}

function _getSidebar($db,$collection){
	APICar::GetSidebar($db,$collection);
}

function _getAllModelsByMake($db,$collection,$make){
	echo json_encode(APICar::GetAllModelsByMake($db,$collection,$make));
}

function _getMenuForFordModels($db,$collection){
	echo json_encode(APICar::GetMenuForFordModels($db,$collection));
}

function _listGetList($db,$collection,$ids){
	echo json_encode(APICar::GetListByIds($db,$collection,$ids));
}








$app->run();
