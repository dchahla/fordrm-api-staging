<?php


function csv_to_array($filename='', $delimiter=',')
{
    if(!file_exists($filename) || !is_readable($filename))
        return FALSE;

    $header = NULL;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== FALSE)
    {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
        {
            if(!$header)
                $header = $row;
            else
                $data[] = array_combine($header, $row);
        }
        fclose($handle);
    }
    return $data;
}

function from_camel_case($input) {
  preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
  $ret = $matches[0];
  foreach ($ret as &$match) {
    $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
  }
  return implode('_', $ret);
}

function getAges($year) {
	$current_year = (int)date("Y");
	$year = (int)$year;
	if ($current_year == $year) {
		return 1;
	} else {
		return $current_year + 1 - $year;
	}
	
}


$vehicles = csv_to_array("data/fordrm_2014-10-13-0738.csv");

$ret = array();
foreach ($vehicles as $idx => $vehicle) {
	$car = array();
	foreach ($vehicle as $key => $val) {
		// Special case for ImageURLs
		if ($key == "ImageURLs") {
			$prop = "image_urls";
		} else {
			$prop = from_camel_case($key);
		}
		$car[$prop] = $val;
	}
	array_push($ret, $car);
}

$vehicles = $ret;
foreach ($vehicles as $idx => $vehicle) {
	foreach ($vehicle as $key => $val) {
		if ($key == "low_miles") {
			if ($vehicle["mileage"]/getAges($vehicle["year"])< 8000) {
				$vehicles[$idx]["low_miles"] = true;
			} else {
				$vehicles[$idx]["low_miles"] = false;
			}
		}


		if ((preg_match("/one owner/i", $vehicle["vehicle_comments"]) == true) || (preg_match("/oneowner/i", $vehicle["vehicle_comments"]) == true) || (preg_match("/one-owner/i", $vehicle["vehicle_comments"]) == true) ) {
			$vehicles[$idx]["one_owner"] = true;
		} else {
			$vehicles[$idx]["one_owner"] = false;
		}

		if ((preg_match("/eco boost/i", $vehicle["vehicle_comments"]) == true) || (preg_match("/ecoboost/i", $vehicle["vehicle_comments"]) == true) || (preg_match("/eco-boost/i", $vehicle["vehicle_comments"]) == true) ) {
			$vehicles[$idx]["ecoboost"] = true;
		} else {
			$vehicles[$idx]["ecoboost"] = false;
		}


		if ($key == "image_urls") {
			$vehicles[$idx]["image_urls"] = explode(",", $vehicles[$idx]["image_urls"]);
		}

		if ($key == "certified") {
			if ($vehicles[$idx]["certified"] == 0) {
				$vehicles[$idx]["certified"] = false;
			} else {
				$vehicles[$idx]["certified"] = true;
			}
		}

		// convert these field to int
		if (in_array($key, array("dealer_id","year","mileage","doors","mpg_city","mpg_hwy","price","days_in_stock"))) {
			$vehicles[$idx][$key] = (int)$vehicles[$idx][$key];
		}

		// unset unused "end" field
		unset($vehicles[$idx]["end"]);



	}
}



// save back to CSV file
file_put_contents("data/vehicles_2014-10-13-0738.json", json_encode($vehicles));
?>